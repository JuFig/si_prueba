﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace html.servicios
{
    // NOTA: si cambia aquí el nombre de interfaz "IServiciodePrueba", también debe actualizar la referencia a "IServiciodePrueba" en Web.config.
    [ServiceContract]
    public interface IServiciodePrueba
    {
        [OperationContract]
        void DoWork();
    }
}
